import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Arrays;
public class Test{
	public static void main(String[] args) {
		Bioskop bioskop = new Bioskop("XXI Summarecon Mall Serpong","Serpong","09.00");
		Studio std = new Studio("Studio 1",100);
		Movie mov = new Movie("Mortal Kombat",35000);
		String[] securityGears = {"Tazer","Bat","Walkie Talkie"};
		String[] cleaningGears = {"Mop","Rub","Glass Cleaner"};
		String[] certification = {"Security trained certification", "Health certification"};
		Employee security = new Security(securityGears,certification);
		security.name="Bram";
		security.startDate= LocalDate.now();
		security.salary=2000000;
		security.startWorkingHour=24;
		security.endWorkingHour=06;
		security.uniform="Satpam";
		Employee cleaning = new Cleaning(cleaningGears);
		cleaning.name="Chandra";
		cleaning.startDate= LocalDate.now();
		cleaning.salary=3000000;
		cleaning.startWorkingHour=07;
		cleaning.endWorkingHour=10;
		cleaning.uniform="Chef";
		Employee cashier = new Cashier(50);
		cashier.name="Daniel";
		cashier.startDate= LocalDate.now();
		cashier.salary=3000000;
		cashier.startWorkingHour=07;
		cashier.endWorkingHour=10;
		cashier.uniform="Cashier";
		LinkedList<Employee> employee = new LinkedList<Employee>();
		employee.add(security);
		employee.add(cleaning);
		employee.add(cashier);
		System.out.println("Data Bioskop");
		System.out.println("Nama Bioskop = "+bioskop.name);
		System.out.println("Alamat Bioskop = "+bioskop.address);
		System.out.println("Jam Operasional = "+bioskop.openingHours);
		System.out.println();
		System.out.println("Data Pegawai :");
		Iterator<Employee> it = employee.iterator();
    	while(it.hasNext()) {
      		Employee i = it.next();
      		if(i instanceof Security) {      		
        		System.out.println("\tSecurity");
        		Security sec = (Security) i;
        		System.out.println("\tPerlengkapan = "+Arrays.toString(sec.securityGears));
        		System.out.println("\tSertifikat = "+Arrays.toString(sec.certification));
      		}else if(i instanceof Cashier){
      			System.out.println("\tCashier");
      			Cashier cas = (Cashier) i;
        		System.out.println("\tPenjualan Tiket = "+cas.hasilPenjualanTicket);
      		}else {
      			System.out.println("\tCleaning");
      			Cleaning cle = (Cleaning) i;
        		System.out.println("\tPerlengkapan = "+Arrays.toString(cle.cleaningGears));
      		}
      		System.out.println("\tNama = "+i.name);
			System.out.println("\tTanggal Masuk = "+i.startDate);
			System.out.println("\tGaji = "+i.salary);
			System.out.println("\tJam Masuk = "+i.startWorkingHour);
			System.out.println("\tJam Keluar = "+i.endWorkingHour);
			System.out.println("\tSeragam = "+i.uniform);
			System.out.println();
    	}
    	System.out.println("Data Studio");
    	System.out.println("Studio = "+std.name);
    	System.out.println("Kursi = "+std.seatsAmount);
    	System.out.println();
    	System.out.println("Data Movie");
    	System.out.println("Movie = "+mov.title);
    	System.out.println("Harga = "+mov.price);

		
		

	}
}